import React from 'react';
import {Layout, Spin} from 'antd';
import CustomHeader from "../components/header/custom-header";
import CustomContent from "../components/content/custom-content";
import {usePromiseTracker} from "react-promise-tracker";
import CustomFooter from "../components/footer/custom-footer";

const MainView = () => {

    const { promiseInProgress } = usePromiseTracker();

    return (
        <Layout >
            <CustomHeader />
            <Spin spinning={promiseInProgress}>
                <CustomContent/>
            </Spin>
            <CustomFooter/>
        </Layout>
    );
}

export default MainView;