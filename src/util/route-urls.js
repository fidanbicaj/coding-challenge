const HOME = '/users';
const USER_DETAILS = `${HOME}/:id`;

const USER_ROUTE = {
    HOME,
    USER_DETAILS
}

export default {... USER_ROUTE};