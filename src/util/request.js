import axios from "axios";


const ApiRequest = axios.create({
    baseURL: 'https://api.github.com/'
});

ApiRequest.interceptors.response.use(response => response, error => error.response);

const request = async(url, type) => {
    const requestConf = {
        method: type,
        url: url
    };

    const result = await ApiRequest.request(requestConf);
    const { data, status } = result;

    return { result: data, status }
}

export default request;
