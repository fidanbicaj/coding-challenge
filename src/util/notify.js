import {notification} from "antd";

const NOTIFICATION_TYPE = {
    SUCCESS: {
        key: 'success',
        value: 'Success'
    },
    ERROR: {
        key: 'error',
        value: 'Error'
    }
};


const notify = (notificationType, description) => {
    const {key, value} = notificationType;
    let notificationMessage = {placement: 'topRight'};
    notificationMessage.message = value;
    notificationMessage.description = description;
    notification[key](notificationMessage);
}

const success = (description) => notify(NOTIFICATION_TYPE.SUCCESS, description);
const error = (description) => notify(NOTIFICATION_TYPE.ERROR, description);

export default  {success, error};

