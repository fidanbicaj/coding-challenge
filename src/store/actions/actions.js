import {CALL_API} from "../middleware/api";
import * as requestUrls from  '../../util/request-urls';
import {RequestMethod} from "../../util/reques-type";

export const SEARCH_USERS = {
    actionType: 'SEARCH_USERS',
    type: 'GET'
};

export const searchUsersByUsername =  (username) => {
    return {
        [CALL_API]: {
            endpoint: `${requestUrls.SEARCH_USERS}?q=${username}`,
            requestType: RequestMethod.GET,
            actionType: SEARCH_USERS.actionType,
            searchText: username
        }
    }
}
