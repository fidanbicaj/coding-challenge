import request from '../../util/request';
import {trackPromise} from "react-promise-tracker";
import notify from "../../util/notify";

const CALL_API = Symbol('DATA API');

const middleware = () => (next) => async (action) => {

    const ACTION_API = action[CALL_API];
    if (!ACTION_API){
        return next(action);
    }

    const {endpoint, actionType, requestType , searchText} = ACTION_API;

    try {
        const response = await  trackPromise(request(endpoint, requestType ));

        const {result} = response;

        if (response.status !== 200) {
            const {message} = result;
            notify.error(message);
            throw new Error('Invalid response from api')
        }

        return next({type: actionType,result: result, searchText});

    }catch (error) {
        console.error(error);
    }
};

export {middleware as apiMiddleWare, CALL_API};
