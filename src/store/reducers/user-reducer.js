import {SEARCH_USERS} from "../actions/actions";

const initialState = {users: [], searchText: ''};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SEARCH_USERS.actionType:{
            const {result: {items}, searchText} = action;
            return {users: items, searchText}
        }
    }
    return state;
}

export default reducer;