import UserReducer from './reducers/user-reducer';
import {createStore, combineReducers, applyMiddleware} from "redux";
import thunkMiddleware from 'redux-thunk';
import {apiMiddleWare }  from './middleware/api';

const rootReducer = combineReducers({
    userReducer: UserReducer
});

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware, apiMiddleWare ));

export default store;