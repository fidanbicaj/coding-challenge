import React from 'react';
import {Layout, Row} from "antd";
import style from './content.module.css';
import 'antd/dist/antd.css';
import {useSelector} from "react-redux";
import UserList from "../user/user-list";
import {Redirect, Route} from "react-router";
import UserDetails from "../user/user-details";
import routes from "../../util/route-urls";

const {Content} = Layout;

const CustomContent = () => {

    const {users} = useSelector(state => state.userReducer);

    return (
        <Layout>
            <Content className={style.Content}>
                <div className={style.Separator}>
                </div>
                <div className={style.ContentDisplay}>
                    <Row justify="start" type="flex" gutter={16}>
                        <Route exact path={"/"} render={() => <Redirect to={"/users"} /> }/>
                        <Route exact path={routes.HOME} render={() => <UserList users={users}/> }/>
                        <Route exact path={routes.USER_DETAILS} component={UserDetails}/>
                    </Row>
                </div>
            </Content>
        </Layout>
    );

}

export default CustomContent;

