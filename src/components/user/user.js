import React from 'react';
import {Card, Typography} from "antd";
import style from './user.module.css';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import {withRouter} from "react-router";

const {Paragraph} = Typography;


const User = (props) => {

    const {user:{id,avatar_url, login}, match: {url}} = props;
    const image = <img className={style.Image} src={avatar_url}/>;

    return (
        <Card
            className={style.Card}
            cover={image}>
            <Typography>
                <h5> {login} </h5>
                <Paragraph>
                    <Link to={`${url}/${id}`} >
                        View Details
                    </Link>
                </Paragraph>
            </Typography>
        </Card>
    );
}

User.propTypes = {
    user: PropTypes.object
}

export default withRouter(User);