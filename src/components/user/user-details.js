import React from 'react';
import {useSelector} from "react-redux";
import {Col, Form, Input} from "antd";

const UserDetails = (props) => {

    const {users} = useSelector(state => state.userReducer);
    if (users.length <= 0) return null;
    const {match:{params:{id}}, form:{getFieldDecorator}} = props;
    const userId = parseInt(id);
    const {login, score, html_url, type, avatar_url} = users.find(({id}) => userId === id);

    return (
        <Col span={24}>
            <Col span={14}>
                <Form >
                    <Form.Item label="Username">{getFieldDecorator("username",{
                        initialValue: login
                    })(<Input disabled/>)}
                    </Form.Item>
                    <Form.Item label="Score">{getFieldDecorator("score", {
                        initialValue: score
                    })(<Input disabled/>)}
                    </Form.Item>
                    <Form.Item label="HTML Url">
                        {getFieldDecorator("html_url", {
                            initialValue: html_url
                        })(<Input disabled/>)}
                    </Form.Item>

                    <Form.Item label="User Type">{getFieldDecorator("type", {
                        initialValue: type
                    })(<Input disabled/>)}
                    </Form.Item>
                </Form>
            </Col>
            <Col span={8} offset={2}>
                <img src={avatar_url} />
            </Col>
        </Col>
    );
}

const UserDetailsForm = Form.create({ name: 'user_details' })(UserDetails);

export default UserDetailsForm;