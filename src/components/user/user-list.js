import React from 'react';
import User from "./user";
import {useSelector} from "react-redux";

const UserList = (props) =>  {
    const {users} = props;
    return users.map(user => <User user={user}/>);
};

export default UserList;
