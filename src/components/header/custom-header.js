import React from 'react';
import {AutoComplete, Col, Icon, Input, Layout, Menu} from "antd";
import style from "./custom-header.module.css";
import {useDispatch} from "react-redux";
import {searchUsersByUsername} from "../../store/actions/actions";
import {withRouter} from "react-router";
import routes from "../../util/route-urls";


const { Header } = Layout;
const DEBOUNCE_IN_MS = 800;

const CustomHeader = (props) => {

    const dispatch = useDispatch();
    const {history} = props;
    const {location: {pathname}} = history;
    const shouldReplaceRoute = pathname !==  routes.HOME;

    const onSearchHandler = (value) => {
        if (value){
            if (shouldReplaceRoute) history.replace(routes.HOME);
            setTimeout(() => dispatch(searchUsersByUsername(value)), DEBOUNCE_IN_MS)
        }
    }

    return (
        <Header>
            <Menu
                theme="dark"
                mode="horizontal"
                className={style.HeaderMenu}
            >
                <Col span={8} offset={8} >
                    <AutoComplete
                        onSearch={onSearchHandler}
                        className={style.AutoComplete}
                        placeholder="Search">
                        <Input suffix={<Icon type="search" />} />
                    </AutoComplete>

                </Col>
            </Menu>
        </Header>
    )
}

export default withRouter(CustomHeader);