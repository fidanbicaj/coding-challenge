import React from 'react';
import './App.css';
import {Provider} from "react-redux";
import MainView from "./containers/main-view";
import store from './store/redux-store';
import {BrowserRouter} from "react-router-dom";

function App() {
  return (
      <Provider store={store}>
          <BrowserRouter>
              <MainView/>
          </BrowserRouter>
      </Provider>
  );
}

export default App;
